# ipchem_dataretrieval

## Summary
1. [What is ipchem\_dataretrieval?](#what-is-ipchem_dataretrieval)
2. [Overview](#overview)
3. [Installation](#installation)
4. [Available data on IPCHEM site](#available-data-on-ipchem-site)
5. [How to download a data package](#how-to-download-a-data-package)
    * [Download directly from the site](#download-directly-from-the-site)
    * [Download using ipchem\_dataretrieval package](#download-using-ipchem_dataretrieval-package)
6. [Extracting information from JSON strings](#extracting-information-from-json-strings)
    * [Extract all the key value pairs](#extract-all-the-key-value-pairs)
    * [Extract a subset of key value pairs](#extract-a-subset-of-key-value-pairs)
7. [Searching for data packages](#searching-for-data-packages)
    * [Summary file](#summary-file)
    * [Start of exploring the data packages](#start-of-exploring-the-data-packages)
    * [Example: select and filter the datasets](#example-select-and-filter-the-datasets)
    * [Example: select and filter any column from summary file](#example-select-and-filter-any-column-from-summary-file)
    * [Short vesion for `retrieve_info()`](#short-vesion-for-retrieve_info)
    * [Example: partial matching for filtering](#example-partial-matching-for-filtering)
    * [Example: more on filtering](#example-more-on-filtering)
8. [Create URLs and download data packages](#create-urls-and-download-data-packages)
    * [Download data from a single URL](#download-data-from-a-single-url)
    * [Download data from multiple URLs](#download-data-from-multiple-urls)
9. [Metadata](#metadata)

## What is ipchem_dataretrieval?

It is a Python package that helps the users to browse through the data packages available on IPCHEM site and download them.

## Overview

You will learn how to select the necessary information from IPCHEM site and download it in your computer.

To have a concret example, let's consider the user wants to see the concentration of lead 
- in the atmosphere, 
- in Romania,
- during year 2011, 
- in different cities. 

```
from ipchem_dataretrieval import (
    retrieve_chemical_name,
    create_package_url,
    download_files,
)

# check if data is available for the previous conditions
chem_df = retrieve_chemical_name(
    chemical_name="lead",
    media="Atmosphere",
    period=2011,
    location="ROU",
)

# once the data packages were identified, create a valid URLs for download
url_list = create_package_url(chem_df)

# use the URLs and download the data
lead_df = download_files(url_list)

```
Now you have the data frame and you can start analysing the information.

## Installation
To install the package go through the follwing steps:

- in the repository hosted on GitLab open *dist* folder
- click on the file that has *.whl* suffix
- click on *Download*
- locate the file in your computer and copy its address
- create a new Python environment and activate it
- in the Terminal, with the Python environment activated, run
`pip install <path_of_whl_file>` 


## Available data on IPCHEM site

The IPCHEM database contains millions of records with measurement for different chemicals found in all types of environments, all over the globe.

The data is collected by a number of organizations and IPCHEM brings together all data packages, stores them and brings everything to a standard format.

The data is structured hierarchically, starting with highest grouping level:

- substance => in IPCHEM is the analyte measured in a sample. This includes mostly classical chemicals, but includes also analytes such as Particulate Matter (PM 2.5 or PM10) or process-generated materials such as dust. 
- module => thematic area of the measurements 
- media => medium which was sampled and analysed in the measurement   
- dataset => a collection of data, published or curated by a single agent, and available for access or download in one or more representation
- package => a file containing a subset of the data records of a dataset grouped by chemical, media and period. Cumulative packages help downloading whole datasets, divided by period in the case of regular data collections (e.g. EFSAMOPER)
- record => *single measurement* (individual measurement made at one point in time); *aggregated data* (a set of one statistical values (e.g. mean, percentile, etc.), obtained by aggregating several individual measurements)


A summary file with all the information found on the site, can be found here: https://ipchem.jrc.ec.europa.eu/public/IPCHEM_public_dataset_packages.csv

## How to download a data package

### Download directly from the site


A user can retrieve a package by going directly to IPCHEM site: [ipchem.jrc.ec.europa.eu](https://ipchem.jrc.ec.europa.eu/).

On the main page, click the yellow button that says *Search data by Chemical, Media and Country*. 


To replicate the same example as the one in the *Overview* section, a user must:
- write the chemical *lead* in the top box
- select the green round button for *Environment Monitoring Data*
- click on *Metadata Info* under *AIRBASE*


On the next page the user must click on *Data Package* tab. The table provided here can be filtered and sorted until the necessary data package is found. For this example, the user will search for chemical *lead* for year 2011. 

The country is not provided in this table. The information card on the left shows the countries where the information is available.

Pressing the download button, a ZIP file will be downloaded. The ZIP file contains a CSV with data in regards to the selected filters.

Now that the file is available in the computer, the user will start to upload it in the environment and use it.

### Download using ipchem_dataretrieval package

There is faster way to get the same data package directly in your Python environment.

Before a user can download a data package from IPCHEM site, a URL must be available.

Using the same example as above, the user will copy and paste the URL of the ZIP file. 

`download_files()` function is used to download data from one or multiple URLs. If the URL points to a ZIP archive that contains a CSV, the CSV file will be extracted and transformed into a Pandas data frame.

```
from ipchem_dataretrieval import download_files

url_list = ["https://ipchem.jrc.ec.europa.eu/public/AIRBASE/2011/IPCHEM_AIRBASE_98304447-a89e-44bf-86be-4061ee55bd76.zip"]

df = download_files(url_list)

print(df.head())
```
```
Chemical Name CAS Number  ... IPCHEM Quality Control                                        SOURCE DATA
0          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
1          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
2          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
3          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
4          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...

[5 rows x 20 columns]
```



## Extracting information from JSON strings

### Extract all the key value pairs

Each CSV file resulting from the archive provided by IPCHEM, contains a column with JSON that stores all the information of a record. 

Although the functions exported by *ipchem_dataretrieval* package are designed to work with data provided by IPCHEM, they can be used for other data sources as long as they follow some restrictions.

One of the restrictions of `download_files()` it that the ZIP archive must contain just one CSV file. Otherwise will return an exception.

The result of the following script is a Pandas data frame that contains a column called *SOURCE DATA*.

Each row from *SOURCE DATA* is a string in JSON format. Some of the keys from this JSON are already transformed into columns in the data frame.

To expand all of them key value pairs from JSON into the initial data frame, a user will use `expand_json()` function. The mandatory argument is the name of the column with JSON strings. 

```
from ipchem_dataretrieval import expand_json

url_list = ["https://ipchem.jrc.ec.europa.eu/public/AIRBASE/2011/IPCHEM_AIRBASE_98304447-a89e-44bf-86be-4061ee55bd76.zip"]


df = download_files(url_list)

df = expand_json(df, json_col = "SOURCE DATA")

print(df.head())
```

To see the difference, the first image is the data frame with *SOURCE DATA* column and the second image is the data frame with the columns expanded from JSON. 

For ease, only the first row was tranposed and displayed.

```
Chemical Name                                                          lead
CAS Number                                                        7439-92-1
Country Code                                                            ESP
Country Name                                                          Spain
Sample Source Code                                                  ES1888A
Sample Source Name                                    ES1888A-CAP EL TEMPLE
Concentration Value                                                   0.002
Unit of Measure                                                       µg/m3
Sampling Date                                                          2011
LOD                                                                     NaN
LOQ                                                                     NaN
Media                                              Atmosphere (Outdoor Air)
Level of Aggregation                                              statistic
Position                                                              exact
Latitude                                                            40.8075
Longitude                                                          0.522222
Location Name                                                       TORTOSA
Related Collection                                                      NaN
IPCHEM Quality Control                                                  NaN
SOURCE DATA               {"Aggregation Period":"year","Statistics Perce...
Name: 0, dtype: object
```

```
Chemical Name                                                                   lead
CAS Number                                                                 7439-92-1
Country Code                                                                     ESP
Country Name                                                                   Spain
Sample Source Code                                                           ES1888A
Sample Source Name                                             ES1888A-CAP EL TEMPLE
Concentration Value                                                            0.002
Unit of Measure                                                                µg/m3
Sampling Date                                                                   2011
LOD                                                                              NaN
LOQ                                                                              NaN
Media                                                       Atmosphere (Outdoor Air)
Level of Aggregation                                                       statistic
Position                                                                       exact
Latitude                                                                     40.8075
Longitude                                                                   0.522222
Location Name                                                                TORTOSA
Related Collection                                                               NaN
IPCHEM Quality Control                                                           NaN
Aggregation Period                                                              year
Statistics Percentage Valid                                                14.521000
Measurement European Group Code                                                  102
Altitude (m)                                                                     2.0
Statistic Name                                                           annual mean
Statistics Number Valid                                                           53
Statistics Average Group                                                         day
Type of Monitoring Station                                                   Traffic
Type of Area                                                                   rural
Measurement Technique Principle    inductive coupled plasma mass spectrometry (IC...
Sampling Matrix                                                              aerosol
Name: 0, dtype: object
```


### Extract a subset of key value pairs

Some data packages contain a large number of rows. Extracting the JSON will add further information, thus expanding the number of columns and increasing the memory footprint.

A user could need only a few of the key value pairs from JSON. In this situation, there is no need to transform all the key value pairs into columns inside the data frame.

Before expanding the JSON strings, the user can explore what are the keys available and what information they contain.

To have a fast script and low memory footprint, the exploration of JSON strings can be done before they are expanded.

Function `json_to_df()` creates a data frame with 3 rows: key name, value and type. 

The script below downloads a data package and transforms it into a data frame. Then the first row from column *SOURCE DATA* is taken and `json_to_df()` is applied. 

```
from ipchem_dataretrieval import json_to_df


url_list = ["https://ipchem.jrc.ec.europa.eu/public/AIRBASE/2011/IPCHEM_AIRBASE_98304447-a89e-44bf-86be-4061ee55bd76.zip"]

df = download_files(url_list)

# keep the first JSON from the data frame
json_sample = df["SOURCE DATA"][0]

json_sample_df = json_to_df(json_sample)

print(json_sample_df)

```
The table below shows 11 key value pairs extracted from JSON.

```
                                                                             value           type
node
Aggregation Period                                                            year  <class 'str'>
Statistics Percentage Valid                                              14.521000  <class 'str'>
Measurement European Group Code                                                102  <class 'str'>
Altitude (m)                                                                   2.0  <class 'str'>
Statistic Name                                                         annual mean  <class 'str'>
Statistics Number Valid                                                         53  <class 'str'>
Statistics Average Group                                                       day  <class 'str'>
Type of Monitoring Station                                                 Traffic  <class 'str'>
Type of Area                                                                 rural  <class 'str'>
Measurement Technique Principle  inductive coupled plasma mass spectrometry (IC...  <class 'str'>
Sampling Matrix                                                            aerosol  <class 'str'>
```


Assuming that the user needs only 3 columns, the script for generating the data can continue as follows.

```
keep_nodes = ["Altitude (m)", "Statistic Name", "Sampling Matrix"]

df = expand_json(df, json_col = "SOURCE DATA", keep_nodes = keep_nodes)
```

Or if the users knows prior to download what keys to keep.

```
url_list = ["https://ipchem.jrc.ec.europa.eu/public/AIRBASE/2011/IPCHEM_AIRBASE_98304447-a89e-44bf-86be-4061ee55bd76.zip"]

df = download_files(url_list)
df = expand_json(df, json_col = "SOURCE DATA", keep_nodes = keep_nodes)
```

The path to obtain the data frame with the expanded JSON string can be even shorter, because `download_files()` is designed to also extract JSON in one go.

If argument `exp_jsn` is `TRUE` than argument `json_col` becomes mandatory, so it will point to the column with JSON strings. `keep_nodes` can be ommited if the user wants all the data from JSON.

```
df = download_files(url_list, exp_jsn = Tru, json_col = "SOURCE DATA", keep_nodes = keep_nodes)
```

## Searching for data packages

### Summary file

As mentioned in chapter 3, IPCHEM site contains a CSV file that keeps count off all data packages available. For convenience this file will be called *summary file*. 

The address of this file is: [https://ipchem.jrc.ec.europa.eu/public/IPCHEM_public_dataset_packages.csv](https://ipchem.jrc.ec.europa.eu/public/IPCHEM_public_dataset_packages.csv)

The summary file is obtained by a user by calling `load_summary()` function with the argument `return_df` as `True`. This function is also used by the familly of functions `retrieve...`. 

```
from ipchem_dataretrieval import load_summary

chem_df = load_summary(return_df=True)

print(chem_df.head())
```

```
      chemical_name   cas_number  media  ...  filesize records id
0  (-)-bornan-2-one     464-48-2  Water  ...   30294.0       9  1
1  (-)-hexaconazole  221627-82-3  Biota  ...   58743.0      16  2
2  (-)-hexaconazole  221627-82-3  Biota  ...   87041.0      23  3
3  (-)-hexaconazole  221627-82-3  Biota  ...  105271.0      28  4
4  (-)-hexaconazole  221627-82-3  Biota  ...  107303.0      28  5

[5 rows x 10 columns]
```


Before it is delivered to the environment, the summary file suffers a few modification to make easy to work with.

A user can ask why the summary file is not already included in the package? The answer is simple. Because IPCHEM adds very often new data packages. If the summary file was included in the *ipchem_dataretrieval* package, each time the summary file was update, so the package must have been updated and published. Thus creating unnecesary updates. The purpose is to offer the user the newest information as fast as possible and this was the solution.

### Start of exploring the data packages

`retrieve_info()` helps the user to navigate easy through the structure of the summary file and to generate the links that host each data package. It works by applying filters over the summary file and retrieving unique rows.

**THERE IS NO NEED TO HAVE THE SUMMARY FILE BEFORE THE CALL OF `retrieve_info()`**. The function will check if the summary file is present and if not, it will download it.

The output is always a Pandas data frame with unique rows.

The definition of the function starts with:
```
retrieve_info(*args, target: str, **kwargs)
```

`retrieve_info()` has a mandatory argument called `target` which is the columns from the summary file the user wants to explore.

`*args` and `**kwargs` are filters that apply to the summary file. They can be the target or any other column from the table.

`*args` can be a data frame which is the response of another call of `retrieve_info`. Any unnamed argument that is not a data frame will be disregarded.

`**kwargs` can be any column from the summary file with one or a list of values. If it is not a column from the summary, the argument will be disregarded.


### Example: select and filter the datasets

In the summary file the data providers are in the column called *dataset*.

To get a unique list of the datasets, `retrieve_info()` is called with the mandatory argument `target` which will have the value of columns from the summary file.

```
from ipchem_dataretrieval import retrieve_info

res_df = retrieve_info(target = "dataset")

print(res_df)
```

```
                      dataset
0                     EMPODAT
1             ISPRAPESTICIDES
2   ISPRAPESTICIDESAGGREGATED
3        HBM4EUALIGNEDSTUDIES
4                   EFSAMOPER
5                     NAIADES
6                         WFD
7                      AIRMEX
8                  AIRQUALITY
9                WATERQUALITY
10                   EFSAVMPR
```

To make the process faster, rather than using the `target`, the user can call the wrapper function to get the list of datasets.

```
from ipchem_dataretrieval import retrieve_dataset

res_df = retrieve_dataset()
```

Depending on the target there can be a lot of unique results.

The user has filtering option. Taking the same example as in previus chapter, let's assume the user wants to keep only the datasets that contain the string *AIR*. To achieve this a logical expression is constructed.


```
retrieve_dataset(dataset=["AIR"])
```
```
      dataset
0      AIRMEX
1  AIRQUALITY
2    OFFICAIR
3     AIRBASE
```

On it's own, this statement does not offer much information. It is just a confirmation of what is found in the summary table.

### Example: select and filter any column from summary file

Any column from the summary table can appear as the `target` argument in `retrieve_info()`. The following pieces of code provides the user a short overview of what can be selected and filtered.

Taking the example of *media*:

```
from ipchem_dataretrieval import retrieve_media

# without filter
retrieve_info(target = media)

# or
retrieve_media()
```

```
           media
0          Water
1          Biota
2          Waste
3          Human
4  Food and Feed
5     Atmosphere
6           Soil
```

```
# with filter, only media that contains with 'Atm'
retrieve_info(target = media, media=['Atm'])

# or
retrieve_media(media=['Atm'])
```

```
        media
0  Atmosphere
```

### Short vesion for `retrieve_info()`

The next function calls are groupped into a single script. The user can chose the short or the long wrinting funtion that includes *target* argument.

For the the remaining of the presentation, mostly the short version will be used.

```
retrieve_info(target = chemical_name)
retrieve_chemical_name()

retrieve_info(target = cas_number)
retrieve_cas_number()

retrieve_info(target = period)
retrieve_period()

retrieve_info(target = media)
retrieve_media()

retrieve_info(target = dataset)
retrieve_dataset()

retrieve_info(target = location)
retrieve_location()

```

Only 6 columns from the summary table have a dedicated wrapper function: *chemical_name, cas_number, media, dataset, period, location*. All the other columns, *package, filesize, records, id*, have a very large number of unique valus and most of filters applied to them will result in a handful of results. They can still be explored through `retrieve_info`, with the corresponding target.

### Example: partial matching for filtering

The user wants to see if there are records for lead concentration in the atmosphere in Romania. The script looks like this:

```
res_df = retrieve_chemical_name(chemical_name = ["lead"], media="Atmosphere", location=["ROU"])

print(res_df)
```

```
   chemical_name       media                                           location
0           lead  Atmosphere  BGR [8], DNK [12], ESP [48], FIN [2], IRL [4],...
1           lead  Atmosphere  AUT [4], BGR [8], DNK [6], FIN [2], IRL [4], R...
2           lead  Atmosphere      AUT [6], BGR [8], DNK [26], NLD [8], ROU [14]
3           lead  Atmosphere  AUT [6], BGR [6], DNK [16], ESP [92], FIN [2],...
4           lead  Atmosphere  AUT [6], BGR [6], DNK [18], ESP [96], FIN [2],...
5           lead  Atmosphere  AUT [2], BGR [6], DNK [18], GBR [42], IRL [12]...
6           lead  Atmosphere  AUT [8], BGR [10], CZE [50], DNK [16], ESP [88...
7           lead  Atmosphere  AUT [10], BGR [12], CZE [48], DNK [16], ESP [8...
8           lead  Atmosphere  AUT [12], BGR [12], CYP [4], CZE [44], DNK [12...
9           lead  Atmosphere  AUT [14], BGR [12], CYP [4], CZE [132], DNK [1...
10          lead  Atmosphere  AUT [2], BGR [18], CYP [6], CZE [138], DNK [16...
11          lead  Atmosphere  BGR [18], CYP [6], CZE [128], DEU [2], DNK [14...
12          lead  Atmosphere  BEL [6], BGR [18], CYP [6], CZE [128], DEU [2]...
13  lead in pm10  Atmosphere  AUT [238], BEL [6476], BGR [2907], CYP [681], ...
14  lead in pm10  Atmosphere  AUT [244], BEL [5984], BGR [2750], CYP [697], ...
15  lead in pm10  Atmosphere  BEL [5686], BGR [2633], CYP [640], DEU [13288]...
16  lead in pm10  Atmosphere  AUT [244], BEL [4942], BGR [2867], CYP [732], ...
17  lead in pm10  Atmosphere  AUT [61], BEL [4309], BGR [2868], CYP [730], C...
18  lead in pm10  Atmosphere  AUT [61], BEL [4339], BGR [2777], CYP [730], C...
19  lead in pm10  Atmosphere  AUT [60], BEL [4320], BGR [3170], CYP [730], C...
20  lead in pm10  Atmosphere  AUT [61], BEL [4343], BGR [3627], CYP [732], C...
```


The result is a data frame with 20 rows. Each record from *location* column contais the string *ROU* which is the ISO code for Romania. 

As a rule, the first column in the resulted data frame is the `target`. All the other columns are in the order of `**kwargs`.

To filter out rows from the summary file, `retrieve_...()` uses partial matching. For example, argument `location=["ROU"]` looks for values in the *location* that contain *ROU*.

If the argument for location filtering was `location=["ROU", "ITA"]`, then all the values that contain *ROU* or *ITA* will be kept.

This is valid for any argument that contains a list. `retrieve_...()` will search for any value from the list to be present in the column.

The result are the columns found at the intersection of all the filters. 

Regarding *location*, one data package contains multiple countries. There is no possibility to filter out a data package that contains one sigle country. There are only a few exceptions to this.

### Example: more on filtering


The user wants to select all chemicals that contain *lead*:

```
res_df = retrieve_chemical_name(chemical_name=["lead"])

print(res_df)
```

```
                    chemical_name
0                            lead
1  lead (and inorganic compounds)
2                       lead (pb)
3          lead and its compounds
4                    lead in pm10
5                   lead in pm2.5
6                     lead in tsp
7                 tetraethyl lead
```

The `target` argument is implicit and has the value *chemical_name*. 

`*args` accepts as valid only a data frame. It can be the output of the `retrieve_...()` or another data frame which columns will be interpreted as filter.

Taking the result of the previuos scripts, the user wants to see all the datasets that have records for lead.

```
res_df = retrieve_dataset(res_df)

print(res_df)
```

```
             dataset                   chemical_name
0            AIRBASE                            lead
1         AIRQUALITY                            lead
2        EMODNETCHEM                            lead
3            EMPODAT                            lead
4             ESBUBA                            lead
5       WATERBASETCM                            lead
6               CHMS                            lead
7   HBM4EUAGGREGATED                            lead
8              PROBE                            lead
9    PROBEAGGREGATED                            lead
10           BIOSOIL                            lead
11              FATE                            lead
12               WFD  lead (and inorganic compounds)
13  EFSACONTAMINANTS                       lead (pb)
14          EFSAVMPR                       lead (pb)
15      WATERQUALITY          lead and its compounds
16           AIRBASE                    lead in pm10
17        AIRQUALITY                    lead in pm10
18           AIRBASE                   lead in pm2.5
19        AIRQUALITY                   lead in pm2.5
20           AIRBASE                     lead in tsp
21        AIRQUALITY                     lead in tsp
22           EMPODAT                 tetraethyl lead
```



The user wants to filter all the chemicals that contain *lead* from dataset *EMPODAT*.

```
res_df = retrieve_chemical_name(chemical_name=["lead"], dataset=["EMPODAT"])

print(res_df)
```

```
     chemical_name  dataset
0             lead  EMPODAT
1  tetraethyl lead  EMPODAT
```


The user wants to filter all the chemicals that contain *lead* from datasets *EMPODAT* and *ESBUBA*, during year *2015*.

```
res_df = retrieve_chemical_name(chemical_name=["lead"], dataset = ["ESBUBA","EMPODAT"], period = [2015])

print(res_df)
```

```
  chemical_name  dataset period
0          lead   ESBUBA   2015
1          lead  EMPODAT   2015
```

Each named argument, other than `target` will be searched using partial matching. For example, the argument `dataset = ["ESBUBA","EMPODAT"]` will search through the *dataset* column if it contains the values *ESBUBA* or *EMPODAT*.

A side effect is that the user can search also partial names, like `dataset = ["ESB","EMP"]`. The user must be aware that other datasets might also contain the string *ESB* or *EMP*.

```
res_df = retrieve_chemical_name(chemical_name=["lead"], dataset = ["ESB","EMP"], period = [2015])

print(res_df)
```

```
  chemical_name  dataset period
0          lead   ESBUBA   2015
1          lead  EMPODAT   2015
```

There is a special function for retrieving available data. Because in almost all cases the *chemical_name* comes along with a unique and non empty *cas_number*, it is convenient to receive both of them in one call. Function ```retrieve_chem_cas()``` has two implict targets which are self explanatory. When calling it, from the start the user receives two columns, rather than one.   

## Create URLs and download data packages

To download a data package the users needs the URL where it is stored on IPCHEM. Remember that the recipe to create the url for a packages is: http://ipchem.jrc.ec.europa.eu/public/{DATASET}/{PERIOD}/{PACKAGE} .

Following the previous example where the user searches for concentration of lead in the atmosphere, it is time to generate the URLs. 

`create_package_url()` takes as argument the result of `retrieve_info()`. It works very simillar with `retrieve_info()` by accepting only `*args` and `**kwargs` as filters. The output is a list of valid URL where the data packages are stored.

```
res_df = retrieve_chemical_name(chemical_name = ["lead"], media="Atmosphere", location=["ROU"])

url_list = create_package_url(res_df)

print(url_list[:2])
```

```
['https://ipchem.jrc.ec.europa.eu/public/AIRBASE/1999/IPCHEM_AIRBASE_f04c9a87-d152-4cc6-804a-993106e15e09.zip', 'https://ipchem.jrc.ec.europa.eu/public/AIRBASE/2000/IPCHEM_AIRBASE_4256796d-e3e0-4e92-9c54-225aac833358.zip'] 
```


Now the user has the URLs for the selection criteria. The data can be download in 2 ways:
- mannualy copy-paste the URLs and go in a browser
- using the function `download_files()`

The last option is for sure the most appealing because it is fast and easy to implement.

### Download data from a single URL

If the list of URLs contains a single URL, then the result will be a data frame. Also, `download_files()` takes as argument a string. 

```
df = download_files(url_list[0])

print(df.head())
```

```
  Chemical Name CAS Number  ... IPCHEM Quality Control                                        SOURCE DATA
0          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
1          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
2          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
3          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
4          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...

[5 rows x 20 columns]
```

The output is a data frame.

The user can go further with the example and expand also the JSON strings while keeping only a few key value pair. Remember that in chapter 5 we showed how to explore the information contained in JSON. That process applies here also. Let't assume the user already know the keys.

```
nodes = ["Sampling Matrix", "Type of Area"]

df = download_files(url_list[0], exp_jsn=True, json_col='SOURCE DATA', keep_nodes=nodes)

# print the column names
print(df.columns)
```

```
Index(['Chemical Name', 'CAS Number', 'Country Code', 'Country Name',
       'Sample Source Code', 'Sample Source Name', 'Concentration Value',
       'Unit of Measure', 'Sampling Date', 'LOD', 'LOQ', 'Media',
       'Level of Aggregation', 'Position', 'Latitude', 'Longitude',
       'Location Name', 'Related Collection', 'IPCHEM Quality Control',
       'Type of Area', 'Sampling Matrix'],
      dtype='object')
```

The last two columns are the ones extracted from JSON.

### Download data from multiple URLs

All the above applies also to a list of URLs that contains more than one URL. The only difference is the output, which is a dictionary of data frames. The keys are names of the data packages. Expanding the JSON is available, but it requires a bit of attention.

```
dict_df = download_files(url_list[:2])

for k, v in dict_df.items():
    print(k)
    print(v.head(), "\n")
```

```
IPCHEM_AIRBASE_f04c9a87-d152-4cc6-804a-993106e15e09
  Chemical Name CAS Number  ... IPCHEM Quality Control                                        SOURCE DATA
0          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
1          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
2          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
3          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
4          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...

[5 rows x 20 columns]

IPCHEM_AIRBASE_4256796d-e3e0-4e92-9c54-225aac833358
  Chemical Name CAS Number  ... IPCHEM Quality Control                                        SOURCE DATA
0          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
1          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
2          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
3          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...
4          lead  7439-92-1  ...                    NaN  {"Aggregation Period":"year","Statistics Perce...

[5 rows x 20 columns]
```

To expand the JSON columns in one go from multiple data packages, the user must pay attention because not all the data packages contain the same key value pair in JSON. For example, *AIRBASE* and *AIRQUALITY* store different information in JSON.


```
url_list = [
    "https://ipchem.jrc.ec.europa.eu/public/AIRQUALITY/2015/IPCHEM_AIRQUALITY_75600abc-35dd-402b-ac9e-c8854e30e7b8.zip",
    "https://ipchem.jrc.ec.europa.eu/public/AIRBASE/2011/IPCHEM_AIRBASE_98304447-a89e-44bf-86be-4061ee55bd76.zip"
]

dict_df = download_files(url_list)

for k, v in dict_df.items():
    print(k)
    print(v['SOURCE DATA'][0], "\n")

```

```
IPCHEM_AIRQUALITY_75600abc-35dd-402b-ac9e-c8854e30e7b8
{"AirQualityStationArea":"rural-regional","UnitOfMeasurement":"µg\/m3","AirQualityStation":"STA-DK0012R","Sample":"SPO_F-DK0012R_00012_100_100","DatetimeBegin":"2015-03-26 00:00:00 +01:00","AirPollutant":"Pb","Verification":"1","Concentration":"0.0020000000","Countrycode":"DK","Namespace":"DK.NERI.AQ","AirQualityStationType":"background","AirQualityStationEoICode":"DK0012R","Validity":"2","AirPollutantCode":"http:\/\/dd.eionet.europa.eu\/vocabulary\/aq\/pollutant\/12","AveragingTime":"day","AirQualityNetwork":"NET-DK004A","MeasurementType":"active","SamplingProcess":"SPP-DK_N_ICP-MS_danishLMP-FP-ELEM","SamplingPoint":"SPO-DK0012R_00012_100","DatetimeEnd":"2015-03-27 00:00:00 +01:00"}

IPCHEM_AIRBASE_98304447-a89e-44bf-86be-4061ee55bd76
{"Aggregation Period":"year","Statistics Percentage Valid":"14.521000","Measurement European Group Code":"102","Altitude (m)":"2.0","Statistic Name":"annual mean","Statistics Number Valid":"53","Statistics Average Group":"day","Type of Monitoring Station":"Traffic","Type of Area":"rural","Measurement Technique Principle":"inductive coupled plasma mass spectrometry (ICP-MS)","Sampling Matrix":"aerosol"}
```

The JSON can be expanded by using the argument `exp_json` and `json_col`. There will be no problems here, each data frame will be stored separate.

If `keep_nodes` is present, it must contain at least a key from each dataset.

The user wants to download the data packages and expand the JSON column. Only 2 keys will be kept and both of them are from *AIRQUALITY* dataset.

The script will return an error because *AIRBASE* does not contain the keys.

```
nodes = ["UnitOfMeasurement", "Concentration"]

url_list = [
    "https://ipchem.jrc.ec.europa.eu/public/AIRQUALITY/2015/IPCHEM_AIRQUALITY_75600abc-35dd-402b-ac9e-c8854e30e7b8.zip",
    "https://ipchem.jrc.ec.europa.eu/public/AIRBASE/2011/IPCHEM_AIRBASE_98304447-a89e-44bf-86be-4061ee55bd76.zip"
]

dict_df = download_files(url_list, exp_jsn=True, json_col = "SOURCE DATA", keep_nodes = nodes)
```

```
raise ValueError("The nodes to keep are not present in the JSON")
ValueError: The nodes to keep are not present in the JSON
```

The solution is to provide keys for both data packages. In some situation, the same information has a different name from one dataset to another.

```
nodes = ["UnitOfMeasurement", "Concentration", "Statistic Name", "Statistics Percentage Valid"]

dict_df = download_files(url_list, exp_jsn=True, json_col = "SOURCE DATA", keep_nodes = nodes)

for k, v in dict_df.items():
    print(k)
    print(v.head(), "\n")
```
```
IPCHEM_AIRQUALITY_75600abc-35dd-402b-ac9e-c8854e30e7b8
  Chemical Name  CAS Number  ... UnitOfMeasurement Concentration
0          lead         NaN  ...             µg/m3  0.0020000000
1          lead         NaN  ...             µg/m3  0.0020000000
2          lead         NaN  ...             µg/m3  0.0030000000
3          lead         NaN  ...             µg/m3  0.0010000000
4          lead         NaN  ...             µg/m3  0.0000000000

[5 rows x 21 columns]

IPCHEM_AIRBASE_98304447-a89e-44bf-86be-4061ee55bd76
  Chemical Name CAS Number Country Code  ... IPCHEM Quality Control Statistics Percentage Valid Statistic Name    
0          lead  7439-92-1          ESP  ...                    NaN                   14.521000    annual mean    
1          lead  7439-92-1          CYP  ...                    NaN                   66.849000    annual mean    
2          lead  7439-92-1          ROU  ...                    NaN                   98.082000    annual mean    
3          lead  7439-92-1          POL  ...                    NaN                   33.151000    annual mean    
4          lead  7439-92-1          POL  ...                    NaN                   43.562000        maximum    

[5 rows x 21 columns]
```

If a user provides a set of keys to keep, than at least one key must be found in each data package. The keys that are not found are skipped. But if all of the keys are not found, it will return an error.

As a warning, the user must be carreful at the amount of data it will download. Once started, the process will end when all the data is downloaded or when the computer is out of memory.

## Metadata

There is another place where the user can search information about the data offered on the site. 

Each dataset has a prospect called *metadata information*. It is a JSON file that details how and where the data was collected.

To obtain the metadata JSON file for a specific data set, the user can call the function `retrieve_meta()`. 

As input, the function takes the name of one or multiple data sets. The output is a dictionary. If the name of a data set cannot be found, the function will skip it and will continue with the rest of the names.

```
from ipchem_dataretrieval import retrieve_meta

dts = ["AIRBASE", "NAIADES"]

jsn = retrieve_meta(dts)
```