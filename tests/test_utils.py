import pytest
import pandas as pd
from ipchem_dataretrieval.utils import (
    clean_name,
    load_summary,
    checkin,
    expand_args,
    retrieve_info,
    retrieve_cas_number,
    retrieve_chemical_name,
    retrieve_dataset,
    retrieve_location,
    retrieve_media,
    retrieve_period,
    retrieve_chem_cas,
    create_package_url,
    retrieve_meta,
    na_to_str,
)


def test_clean_name():
    nm = "AbC--123_dEf,."
    assert clean_name(nm) == "abc_123_def"


# @pytest.mark.skip(reason="skip for now")
def test_load_summary():
    df = load_summary()
    assert df is None

    df = load_summary(return_df=True)
    assert len(df) > 0


def test_checkin():
    assert checkin("test", "test") is True
    assert checkin("testing", "test") is True
    assert checkin("test", "testing") is False
    assert checkin("test", ["te", "st"]) is True
    assert checkin("test", ["te", "st"], type="all") is True
    assert checkin("test", ["te", "ts"], type="all") is False
    assert checkin(123, 12) is False
    assert checkin(123, 123) is True
    assert checkin(123, [123, 456]) is True
    assert checkin(123, [123, 456], type="all") is False
    assert checkin(None, "test") is True
    assert checkin("test", None) is False


def test_expand_args():

    df = pd.DataFrame({"a": [1, 2], "b": [3, 4]})

    assert expand_args() == {}
    assert expand_args(a=1) == {"a": [1]}
    assert expand_args(a=1, b=[1, 2]) == {"a": [1], "b": [1, 2]}
    assert expand_args(df) == {"a": [1, 2], "b": [3, 4]}
    assert expand_args(df, a=1) == {"a": [1], "b": [3, 4]}
    assert expand_args(df, c=[5, 6]) == {"a": [1, 2], "b": [3, 4], "c": [5, 6]}


def test_retrieve_info_error():
    with pytest.raises(ValueError) as ex:
        retrieve_info(target="abc")


def test_retrieve_info_one_target():
    df = retrieve_info(target="media")
    assert df.shape[1] == 1


def test_retrieve_info_one_target_one_filter():
    df = retrieve_info(dataset="EMPODAT", target="media")
    assert df.shape[1] == 2

    filter = pd.DataFrame({"dataset": ["EMPODAT"]})
    df = retrieve_info(filter, target="media")
    assert df.shape[1] == 2


def test_retrieve_info_partial_filter():
    df = retrieve_info(dataset="EMP", target="media")
    assert df.shape[1] == 2


def test_retrieve_info_innexistent_filter():
    df = retrieve_info(test="EMP", target="media")
    assert df.shape[1] == 1


def test_retrieve_info_no_match_filter():
    df = retrieve_info(dataset="XYZ", target="media")
    assert len(df) == 0
    assert len(df.columns) == 2


def test_retrieve_info_chain():
    assert retrieve_info(retrieve_info(target="media"), target="dataset").shape[1] == 2


def test_wrappers():
    assert retrieve_cas_number().shape[1] == 1
    assert retrieve_chemical_name().shape[1] == 1
    assert retrieve_dataset().shape[1] == 1
    assert retrieve_location().shape[1] == 1
    assert retrieve_media().shape[1] == 1
    assert retrieve_period().shape[1] == 1


def test_create_package_url():
    filter = {"cas_number": "68291-97-4"}
    url = create_package_url(**filter)
    assert isinstance(url, list)
    assert len(url) >= 1


def test_retrieve_meta():
    meta = retrieve_meta("XYZ")
    assert len(meta) == 0

    meta = retrieve_meta("EMPODAT")
    assert len(meta) == 1

    meta = retrieve_meta(["EMPODAT", "XYX"])
    assert len(meta) == 1


def test_na_to_str_with_string():
    result = na_to_str("test")
    assert result == "test", f"Expected 'test', got {result}"


def test_na_to_str_with_number():
    result = na_to_str(123)
    assert result == 123, f"Expected 123, got {result}"


def test_na_to_str_with_none():
    result = na_to_str(None)
    assert result == "", f"Expected '', got {result}"


def test_na_to_str_with_na():
    result = na_to_str(pd.NA)
    assert result == "", f"Expected '', got {result}"


def test_retrieve_chem_cas():
    result = retrieve_chem_cas()
    assert result.shape[1] == 2
