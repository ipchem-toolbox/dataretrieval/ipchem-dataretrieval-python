import pytest
import pandas as pd
from ipchem_dataretrieval.download_util import (
    get_url_suffix,
    get_content,
    url_to_df,
    urls_to_df,
)

"""
download utils tests
"""


@pytest.fixture
def url():
    return "https://ipchem.jrc.ec.europa.eu/public/AIRBASE/1993/IPCHEM_AIRBASE_b46fcc19-0f9f-4889-b805-eae57c775c13.zip"


@pytest.fixture
def urls():
    return [
        "https://ipchem.jrc.ec.europa.eu/public/AIRBASE/1993/IPCHEM_AIRBASE_b46fcc19-0f9f-4889-b805-eae57c775c13.zip",
        "https://ipchem.jrc.ec.europa.eu/public/AIRBASE/1994/IPCHEM_AIRBASE_41c66007-d39f-417a-92e8-74cfd7f35384.zip",
    ]


def test_get_url_suffix():

    url = "https://www.example.com/test.txt"
    assert get_url_suffix(url) == "txt"

    url = "https://www.example.com/test.txt/"
    assert get_url_suffix(url) == "txt"

    url = "https://www.example.com/test.txt/test"
    assert get_url_suffix(url) == "test"

    url = "https://www.example.com/test.txt/test/"
    assert get_url_suffix(url) == "test"


def test_get_content(url):
    assert isinstance(get_content(url), bytes)


def test_url_to_df(url):
    assert isinstance(url_to_df(url), pd.DataFrame)
    assert isinstance(
        url_to_df(url, exp_jsn=True, json_col="SOURCE DATA"), pd.DataFrame
    )
    assert isinstance(
        url_to_df(
            url,
            exp_jsn=True,
            json_col="SOURCE DATA",
            keep_nodes=["Aggregation Period"],
        ),
        pd.DataFrame,
    )


def test_urls_to_df(urls):
    assert isinstance(urls_to_df(urls), dict)
    assert isinstance(urls_to_df(urls, exp_jsn=True, json_col="SOURCE DATA"), dict)
    assert isinstance(
        urls_to_df(
            urls,
            exp_jsn=True,
            json_col="SOURCE DATA",
            keep_nodes=["Aggregation Period"],
        ),
        dict,
    )
