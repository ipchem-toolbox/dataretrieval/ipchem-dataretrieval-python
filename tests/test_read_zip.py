from io import BytesIO
from zipfile import ZipFile

from ipchem_dataretrieval.read_zip import read_zip_pack


def test_bytes_pack():
    csv = "A,B,C\n1,2,3\n4,5,6\n7,8,9\n".encode()

    mf = BytesIO()
    with ZipFile(mf, "w") as zp:
        zp.writestr("df.csv", csv)

    df = read_zip_pack(mf.getvalue())
    assert df.shape == (3, 3)
