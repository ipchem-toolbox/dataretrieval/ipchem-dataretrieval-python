import pytest
import pandas as pd
from ipchem_dataretrieval.json_util import expand_json, json_to_df


@pytest.fixture
def df_with_json_col():
    return pd.DataFrame(
        {
            "A": [1, 2, 3],
            "B": [4, 5, 6],
            "json_col": [
                '{"a": 1, "b": 2, "c": 3}',
                '{"a": 4, "b": 5, "c": 6}',
                '{"a": 7, "b": 8, "c": 9}',
            ],
        }
    )


@pytest.fixture
def list_with_json():
    return [
        '{"a": 1, "b": 2, "c": 3}',
        '{"a": 4, "b": 5, "c": 6}',
        '{"a": 7, "b": 8, "c": 9}',
    ]


@pytest.fixture
def single_json():
    return '{"a": 1, "b": 2, "c": 3}'


def test_expand_json_df(df_with_json_col):
    res = expand_json(df_with_json_col, json_col="json_col")
    assert res.shape == (3, 5)


def test_expand_json_list(list_with_json):
    res = expand_json(list_with_json)
    assert len(res) == 3


def test_json_to_df(single_json):
    res = json_to_df(single_json)
    assert res.shape == (3, 2)
