#Required module/functions:
from ipchem_dataretrieval import (
    retrieve_chemical,
    retrieve_media,
    retrieve_period,
)

#1. User code knows already the media, location and period (e.g. ‘Food and Feed’, ‘ITA’, ‘2018’), 
#or it can retrieve the lists of possible values by using the specific helper functions:

#a. media field
print(retrieve_media())

#b. period field for a given media
print(retrieve_period(media = "Food and Feed"))

#3. User code searches for the chemicals available for certain media, period and location 
#(e.g. all results available for "Food and Feed" media, collected in Italy after 2018).
print(retrieve_chemical(media = "Food and Feed", period = [2018,2019,2020,2021], location = "ITA"))

#Note: the output of each steps can be saved in a data frame and CSV file:
df = retrieve_chemical(media = "Food and Feed", period = [2018,2019,2020,2021], location = "ITA")
df.to_csv('chem_list.csv')