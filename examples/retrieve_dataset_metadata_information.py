#Required module/functions:
import json
from ipchem_dataretrieval import (
    retrieve_dataset,
    retrieve_meta,
)

#1. User code knows already the datasets (e.g. 'EFSAMOPER','EFSAVMPR'), 
#or it can retrieve the lists of possible values by using the specific helper functions:

#a. the full list of datasets
print(retrieve_dataset())

#b. the list of dataset filtered for chemical, media, location and/or period (e.g. media is "Food and Feed")
print(retrieve_dataset(media = "Food and Feed"))

#3. User code requests metadata for the desired datasets
print(retrieve_meta(["EFSAMOPER","EFSAVMPR"]))

#Note: the metadata information can be saved in a dictionary and JSON file
meta_list = retrieve_meta(["EFSAMOPER","EFSAVMPR"])
with open('metadata.json', 'w') as f:
    json.dump(meta_list, f)