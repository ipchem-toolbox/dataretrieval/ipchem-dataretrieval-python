#Required module/functions:
import pandas as pd
from ipchem_dataretrieval import (
    retrieve_chemical,
    retrieve_cas,
    retrieve_dataset,
)

#1. User code knows already the chemical and the cas number 
#(e.g. ‘carboxin’, ‘5234-68-4’), or it can retrieve the lists of possible values by using the specific helper functions:

#a. by name:
print(retrieve_chemical())

#b. by CAS Number:
print(retrieve_cas())

#3. User code searches the datasets for:

#a. single chemical or a substring (e.g. the chemical name is carboxin)
print(retrieve_dataset(chemical_name = "carboxin"))

#b. CAS Number (e.g. the CAS Number is 5234-68-4)
print(retrieve_dataset(cas_number = "5234-68-4"))

#c. list of chemicals:
print(retrieve_dataset(chemical_name = ["aldrin","carboxin","dieldrin"]))

#d.chemicals from CSV file:
#Note: the path must locate an existent file
df = pd.read_csv("chemical_to_search.csv")
print(retrieve_dataset(chemical_name = df["chemical_name"]))

#Note: the output of each steps can be saved in a data frame and CSV file
dataset_df = retrieve_dataset(chemical_name = ["aldrin","carboxin","dieldrin"])
dataset_df.to_csv("dataset_found.csv")