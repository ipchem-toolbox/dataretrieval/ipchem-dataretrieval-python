#Required module/functions:
import pandas as pd
from ipchem_dataretrieval import (
    create_package_url,
    download_files,
    expand_json,
    retrieve_chemical,
)

#1. User code knows already the dataset code, media, location, period and/or chemicals 
#(e.g. all data packages for fenuron in "Food and Feed" media, collected in Italy after 2018 and provided by EFSAMOPER dataset)
url_list = create_package_url(retrieve_chemical(chemical_name = "fenuron", period = [2018,2019,2020,2021], media = "Food and Feed", dataset = "EFSAMOPER", location = "ITA"))

#3. User code request to download the filtered data packages

#a. with the SOURCE DATA field in a unique field:
data_dict = download_files(url_list)

#Note: the structure of SOURCE DATA field can be later expanded
data_dict_1 = download_files(url_list[:1])
data_dict_exp = expand_json(data_dict_1, json_col = "SOURCE DATA")

#b. with all the SOURCE DATA field exploited in separate fields:
data_dict_exp = download_files(url_list, exp_jsn=True, json_col = "SOURCE DATA")

#c. with some of the SOURCE DATA field exploited in separate fields:
nodes = ["PROGSAMPSTRATEGY", "PRODPRODMETH"]
data_dict_exp = download_files(url_list[:1], exp_jsn=True, json_col = "SOURCE DATA", keep_nodes = nodes)

#Note: the content of the data packages can be saved in a data frame and CSV file
data_df = pd.concat(data_dict.values(), ignore_index=True)
data_df.to_csv('data_records.csv', index=False)