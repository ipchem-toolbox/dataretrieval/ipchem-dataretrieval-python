#Required module/functions:
import pandas as pd
from ipchem_dataretrieval import (
    create_package_url,
    retrieve_chemical,
)

#1. User code knows already the dataset code, media, location, period and/or chemicals 
#(e.g. all data packages for fenuron in "Food and Feed" media, collected in Italy after 2018 and provided by EFSAMOPER dataset)
filtered_list = retrieve_chemical(chemical_name = "fenuron", period = [2018,2019,2020,2021], media = "Food and Feed", dataset = "EFSAMOPER", location = "ITA")

#3. User code requests the list of data packages URLs for the applied filter
url_list = create_package_url(filtered_list)

#Note: the output can be saved in a data frane and CSV file
url_list = create_package_url(filtered_list)
url_df = pd.DataFrame(url_list)
url_df.to_csv("package.csv", index=False)