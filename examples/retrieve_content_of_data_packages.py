#Required module/functions:
import pandas as pd
from ipchem_dataretrieval import (
    create_package_url,
    download_files,
    retrieve_chemical,
)

#1. User code knows already the dataset code, media, location, period and/or chemicals 
#(e.g. all data packages for fenuron in "Food and Feed" media, collected in Italy after 2018 and provided by EFSAMOPER dataset)
filtered_list = retrieve_chemical(chemical_name = "fenuron", period = [2018,2019,2020,2021], media = "Food and Feed", dataset = "EFSAMOPER", location = "ITA")

#3. User code requests the list of data packages URLs for the applied filter
url_list = create_package_url(filtered_list)

#5. User code requests to download the content of the filtered data packages
data_dict = download_files(url_list)

#Note: the content of the data packages can be saved in a data frame and CSV file
data_df = pd.concat(data_dict.values(), ignore_index=True)
data_df.to_csv('data_content.csv', index=False)