"""
Series of helper function to retrieve data from the IPCHEM database.
"""

import re
import json
from functools import partial
from io import StringIO
import pandas as pd
from typing import Any, Dict, List, Union, Set, List, Iterable
from .download_util import get_content

__all__ = [
    "load_summary",
    "retrieve_info",
    "retrieve_cas_number",
    "retrieve_chemical_name",
    "retrieve_dataset",
    "retrieve_location",
    "retrieve_media",
    "retrieve_period",
    "retrieve_chem_cas",
    "create_package_url",
    "retrieve_meta",
    "clean_name",
    "checkin",
    "na_to_str",
    "expand_args"
]


def clean_name(nm: str) -> str:
    """Pretiffy a string by replacing spaces, dots, hyphens, and underscores with underscores and converting to lowercase.

    Args:
        nm (str): the string to pretiffy

    Returns:
        str: Pretiffied string
    """
    nm = re.sub(r"[\s]|\.{1,}|-{1,}|\,{1,}|_{2,}", "_", nm)
    nm = re.sub(r"_{2,}", "_", nm)
    nm = re.sub(r"_$", "", nm)
    return nm.lower()


def load_summary(return_df: bool = False) -> Union[None, pd.DataFrame]:
    """
    Load chemical summary data from Ipchem site.

    Args:
        nm (str): if False, the function will not return the DataFrame, just put it in global environment of the module.
    Returns:
        pandas.DataFrame: The loaded chemical summary data.
    """
    global chem_summary
    url = "https://ipchem.jrc.ec.europa.eu/public/IPCHEM_public_dataset_packages.csv"
    resp = get_content(url)
    with StringIO(resp.decode("utf-8")) as s:
        chem_summary = pd.read_csv(s, sep=",")
    chem_summary.columns = [clean_name(col) for col in chem_summary.columns]
    chem_summary.dropna(subset=["chemical_name"], inplace=True)
    chem_summary["period"] = chem_summary["period"].apply(
        lambda x: pd.NA if pd.isna(x) else int(x)
    )

    if return_df:
        return chem_summary
    else:
        return None


def checkin(
    var: int | float | str, pattern: int | float | Iterable, type: str = "any"
) -> bool:
    """The function checks if the values that are part of the pattern are found
    in the var. Pattern does not contain any regex.
    A generalization of the "in" operator.

    Args:
        var (int|float|str): where to search for the pattern
        pattern (int|float|Iterable): what values to search. this is not a regex.
        type (str, optional): "any" or "all". default "any". "any" means that any value in the pattern must be found in the var.
    Raises:
        ValueError: "The pattern must be a string, number, list, set"
        ValueError: "The pattern must be all numbers or all strings, depending on the column type"
        ValueError: "The type must be any or all"

    Returns:
        bool: if the pattern is found in the var
    """

    if (
        pd.isna(var) or pd.isnull(var) or var is None
    ):  # use pd because in retrieve is always a pd.dataframe involved
        return True
    if isinstance(pattern, (str, int, float)):
        pattern = [pattern]
    elif isinstance(pattern, Iterable):
        pattern = list(pattern)
    elif pattern is None:
        return False
    else:
        raise ValueError("The pattern must be a string, number, list, set")

    if type == "any":
        if all([isinstance(val, (int, float)) or (val is None) for val in pattern]):
            res = any([val == var for val in pattern])
        elif all([isinstance(val, str) or (val is None) for val in pattern]):
            pattern = [re.escape(val) for val in pattern if val is not None]
            pattern = "|".join(pattern)
            res = False if re.search(pattern, var) is None else True
        else:
            raise ValueError(
                "The pattern must be all numbers or all strings, depending on the column type"
            )
    elif type == "all":
        if all([isinstance(val, (int, float)) for val in pattern]):
            res = all([val == var for val in pattern])
        elif all([isinstance(val, str) for val in pattern]):
            pattern = [re.escape(val) for val in pattern if val is not None]
            pattern = ".*)(.*".join(pattern)
            pattern = "(.*" + pattern + ".*)"
            res = False if re.search(pattern, var) is None else True
        else:
            raise ValueError(
                "The pattern must be all numbers or all strings, depending on the column type"
            )
    else:
        raise ValueError("The type must be any or all")

    return res


def na_to_str(val: Any) -> Any:
    """
    Convert None or np.nan to an empty string.

    Args:
        val (Any): The value to convert.

    Returns:
        Any: The converted value.

    Raises:
        None

    """
    if pd.isna(val) or pd.isnull(val) or val is None:
        return ""
    else:
        return val


def expand_args(*args: Any, **kwargs: Any) -> Dict[str, List[Any]]:
    """
    Expand the arguments into a dictionary.

    Args:
        *args: Variable length arguments. Each argument should be a pandas DataFrame.
        **kwargs: Keyword arguments. Each argument should be a list.

    Returns:
        res_dct (Dict[str, List[Any]]): A dictionary containing the expanded arguments.

    Raises:
        None

    """
    res_dct = {}
    for ar in args:
        if isinstance(ar, pd.DataFrame):
            df_dct = ar.to_dict(orient="list")
            res_dct.update(df_dct)
        else:
            print("The unnamed argument is not a DataFrame. Skipping...")

    for key, val in kwargs.items():
        if isinstance(val, (str, int, float)):
            res_dct[key] = [val]
        elif isinstance(val, Iterable):
            res_dct[key] = list(val)
        else:
            print(f"The argument {key} is not a str, list, set, tuple. Skipping...")

    return res_dct


def retrieve_info(*args, target: str, **kwargs):
    """
    Selects and filters data from a DataFrame based on the specified targets and filters.

    Parameters:
        *args: Additional positional arguments that act as filters.
        targets (str): The column to select from chem_summary.
        **kwargs: Additional keyword arguments to be passed to the `expand_args` function.

    Returns:
        pandas.DataFrame: The filtered DataFrame containing the selected columns.

    Raises:
        ValueError: If the object passed is not a DataFrame or if the target is not a string, set, or list.
        ValueError: If a target column is not present in the DataFrame.

    """

    if "chem_summary" in globals():
        res_df = chem_summary.copy()
    else:
        print("Starting to download the summary file")
        load_summary()
        res_df = chem_summary.copy()

    if not isinstance(target, str):
        raise ValueError("The target must be a string")

    if target not in res_df.columns:
        raise ValueError(f"Column {target} is not in the dataframe")

    filters = expand_args(*args, **kwargs)

    if len(filters) > 0:
        nms = list(filters.keys())
        nms = [nm for nm in nms if nm in res_df.columns]
        if len(nms) > 0:
            mask = [True] * res_df.shape[0]
            for nm in nms:
                temp_mask = [
                    checkin(val, filters[nm], type="any") for val in res_df[nm]
                ]
                mask = [x & y for x, y in zip(mask, temp_mask)]
            res_df = res_df.loc[mask, [target] + nms]
        else:
            res_df = res_df[[target]]
    else:  # no filters
        res_df = res_df[[target]]

    res_df = res_df.drop_duplicates().reset_index(drop=True)
    res_df = res_df.loc[:, ~res_df.columns.duplicated()]
    # res_df.dropna(
    #     subset=target, inplace=True
    # )  # targets must be present. important for create url

    return res_df


retrieve_cas_number = partial(retrieve_info, target="cas_number")
retrieve_chemical_name = partial(retrieve_info, target="chemical_name")
retrieve_dataset = partial(retrieve_info, target="dataset")
retrieve_location = partial(retrieve_info, target="location")
retrieve_media = partial(retrieve_info, target="media")
retrieve_period = partial(retrieve_info, target="period")


def create_package_url(*args, **kwargs) -> List[str]:
    """
    Create package URLs filtering by args and kwargs.

    Returns:
        List: list of valid URLs.

    """
    targets = ["dataset", "period", "package"]

    if "chem_summary" in globals():
        url_df = chem_summary.copy()
    else:
        print("Starting to download the summary file")
        load_summary()
        url_df = chem_summary.copy()

    filters = expand_args(*args, **kwargs)

    if len(filters) > 0:
        nms = list(filters.keys())
        nms = [nm for nm in nms if nm in url_df.columns]
        if len(nms) > 0:
            mask = [True] * url_df.shape[0]
            for nm in nms:
                temp_mask = [
                    True if pd.isna(val) or pd.isnull(val) else val in filters[nm]
                    for val in url_df[nm]
                ]
                mask = [x & y for x, y in zip(mask, temp_mask)]
            url_df = url_df.loc[mask, targets]
        else:
            url_df = url_df[targets]
    else:  # no filters
        url_df = url_df[targets]

    url_df = url_df.drop_duplicates().reset_index(drop=True)
    url_df = url_df.loc[:, ~url_df.columns.duplicated()]

    url_df = url_df.assign(
        url=lambda x: "https://ipchem.jrc.ec.europa.eu/public/"
        + x["dataset"].apply(lambda x: na_to_str(x))
        + "/"
        + x["period"].apply(lambda x: na_to_str(x)).astype(str)
        + "/"
        + x["package"].apply(lambda x: na_to_str(x))
    )

    urls = url_df["url"].to_list()

    return urls


def select_data(targets: Union[str, Set, List], df: pd.DataFrame, *args, **kwargs):
    """
    Selects and filters data from a DataFrame based on the specified targets and filters.

    Parameters:
        targets (str, set, or list): The columns to select from the DataFrame.
        df (pandas.DataFrame): The DataFrame to select data from.
        *args: Additional positional arguments to be passed to the `expand_args` function.
        **kwargs: Additional keyword arguments to be passed to the `expand_args` function.

    Returns:
        pandas.DataFrame: The filtered DataFrame containing the selected columns.

    Raises:
        ValueError: If the object passed is not a DataFrame or if the target is not a string, set, or list.
        ValueError: If a target column is not present in the DataFrame.

    """
    res_df = df.copy()

    if isinstance(res_df, pd.DataFrame) is not True:
        raise ValueError("The object passed is not a DataFrame")

    if isinstance(targets, (set, list)):
        targets = list(targets)
    elif isinstance(targets, str):
        targets = [targets]
    else:
        raise ValueError("The target must be a string, set or list")

    for target in targets:
        if target not in res_df.columns:
            raise ValueError(f"Column {target} is not in the dataframe")

    filters = expand_args(*args, **kwargs)
    # breakpoint()
    if len(filters) > 0:
        nms = list(filters.keys())
        nms = [nm for nm in nms if nm in res_df.columns]
        if len(nms) > 0:
            for nm in nms:
                res_df = res_df.iloc[res_df[nm].isin(filters[nm]).to_list(), :]
            res_df = res_df.loc[:, targets + nms]
        else:
            res_df = res_df[targets]
    else:  # no filters
        res_df = res_df[targets]

    res_df = res_df.drop_duplicates().reset_index(drop=True)
    res_df = res_df.loc[:, ~res_df.columns.duplicated()]
    res_df.dropna(
        subset=targets, inplace=True
    )  # targets must be present. important for create url

    return res_df


def retrieve_meta(dataset: str | List[str]) -> dict:
    """Retrieve the specificiafions of one or multiple datasets from Ipchem site.

    Args:
        dataset (str | list): names or names of the datasets to retrieve.

    Returns:
        dict: specificiations of the datasets.
    """

    meta_dict = {}

    if isinstance(dataset, str):
        dataset = [dataset]
    elif isinstance(dataset, list):
        pass
    else:
        raise ValueError("dataset must be a string or a list of strings.")

    for l in dataset:
        url = f"https://ipchem.jrc.ec.europa.eu/public/{l}/IPCheM_{l}_metadata.json"
        try:
            resp = get_content(url)
        except:
            print(f"The dataset {l} could not be retrieved. Check the url {url}")
        else:
            meta_dict[l] = json.loads(resp.decode("utf-8"))

    return meta_dict


def retrieve_chem_cas(*args, **kwargs):
    """
    Selects and filters the summary file having as targets chemical_name and cas_number.

    Parameters:
        *args: Additional positional arguments that act as filters.
        **kwargs: Additional keyword arguments to be passed to the `expand_args` function.

    Returns:
        pandas.DataFrame: The filtered DataFrame containing the selected columns.

    Raises:
        ValueError: If the object passed is not a DataFrame.

    """
    target = ["chemical_name", "cas_number"]

    if "chem_summary" in globals():
        res_df = chem_summary.copy()
    else:
        print("Starting to download the summary file")
        load_summary()
        res_df = chem_summary.copy()

    filters = expand_args(*args, **kwargs)

    if len(filters) > 0:
        nms = list(filters.keys())
        nms = [nm for nm in nms if nm in res_df.columns]
        if len(nms) > 0:
            mask = [True] * res_df.shape[0]
            for nm in nms:
                temp_mask = [
                    checkin(val, filters[nm], type="any") for val in res_df[nm]
                ]
                mask = [x & y for x, y in zip(mask, temp_mask)]
            res_df = res_df.loc[mask, target + nms]
        else:
            res_df = res_df[target]
    else:  # no filters
        res_df = res_df[target]

    res_df = res_df.drop_duplicates().reset_index(drop=True)
    res_df = res_df.loc[:, ~res_df.columns.duplicated()]

    return res_df
