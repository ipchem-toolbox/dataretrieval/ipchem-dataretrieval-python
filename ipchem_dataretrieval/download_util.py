"""Download content from a given URL.
"""

from urllib import request
from urllib.error import URLError
from typing import List, Union, Dict
import pandas as pd
from .read_zip import read_zip_pack
from .json_util import expand_json

__all__ = ["download_files"]


def get_url_suffix(url: str) -> str:
    """
    Retrieves the suffix of a given URL.

    Args:
        url (str): The URL to retrieve the suffix from.

    Returns:
        str: The suffix of the URL.
    """
    if url.endswith("/"):
        url = url[:-1]

    suffix = url.split("/")[-1]

    suffix = suffix.split(".")[-1]

    return suffix


def get_content(url: str) -> bytes:
    """
    Retrieves the content from a given URL.

    Args:
        url (str): The URL to retrieve content from.

    Returns:
        bytes: The raw content retrieved from the URL.

    Raises:
        URLError: If an error occurs while making the request.
    """
    try:
        resp = request.urlopen(url)
    except Exception as e:
        raise URLError(
            f"The call encountered an error. Check if the URL is working: {url}"
        )

    raw_data = resp.read()

    return raw_data


def url_to_df(
    url: str, exp_jsn: bool = False, json_col: str = None, keep_nodes: list = None
) -> pd.DataFrame:
    """
    Converts a URL pointing to a zip or csv file into a pandas DataFrame.

    Parameters:
    - url (str): The URL pointing to the zip or csv file.
    - exp_jsn (bool): Whether to expand JSON columns in the DataFrame. Default is False.
    - json_col (str): The name of the JSON column to expand. Only applicable if exp_jsn is True.
    - keep_nodes (list): List of JSON nodes to keep during expansion. Only applicable if exp_jsn is True.

    Returns:
    - df (pandas.DataFrame): The DataFrame created from the zip or csv file.

    Raises:
    - ValueError: If the URL does not point to a zip or csv file, or if the file cannot be parsed into a DataFrame.

    """
    suffix = get_url_suffix(url)
    resp = get_content(url)

    if suffix == "csv":
        try:
            df = pd.DataFrame(resp)
        except Exception as e:
            raise ValueError(
                f"The CSV file could not be parsed into a DataFrame:\n\n", {e}
            )
    elif suffix == "zip":
        df = read_zip_pack(resp)
    else:
        raise ValueError(f"The URL must point to a zip or csv file")

    if exp_jsn:
        df = expand_json(df, json_col, keep_nodes)

    return df


def urls_to_df(
    urls: str | List[str],
    exp_jsn: bool = False,
    json_col: bool = None,
    keep_nodes: List[str] = None,
) -> dict[str, pd.DataFrame]:
    """
    Convert a list of URLs to a dictionary of DataFrames.

    Args:
        urls (list): A list of URLs to convert.
        exp_jsn (bool, optional): Whether to expand JSON columns. Defaults to False.
        json_col (str, optional): The name of the JSON column to expand. Defaults to None.
        keep_nodes (list, optional): A list of nodes to keep when expanding JSON columns. Defaults to None.

    Returns:
        dict: A dictionary where the keys are the names of the DataFrames and the values are the DataFrames themselves.
    """

    urls = list(urls)
    n = len(urls)
    df_dict = {}

    print(f"There are {n} urls in the list")

    for url in urls:
        df_name = get_url_suffix(url.removesuffix("." + get_url_suffix(url)))
        df = url_to_df(url, exp_jsn, json_col, keep_nodes)
        df_dict[df_name] = df

    return df_dict


def download_files(
    url: str | List[str],
    exp_jsn: bool = False,
    json_col: str = None,
    keep_nodes: list = None,
) -> Union[pd.DataFrame, Dict[str, pd.DataFrame]]:
    """
    Download files from the URLs in the urls attribute.

    Args:
        url (str or list): The URL(s) to download files from.
        exp_jsn (bool, optional): Whether to expand JSON files. Defaults to False.
        json_col (str, optional): The column containing JSON data. Defaults to None.
        keep_nodes (list, optional): The nodes to keep when expanding JSON. Defaults to None.

    Returns:
        Pandas dataframe or dictionary of dataframes

    Raises:
        ValueError: If no URLs are found or the URL list has length 0.

    """
    if url is None:
        raise ValueError("No URLs found")

    if len(url) == 0:
        raise ValueError("The URL list has length 0.")

    if isinstance(url, str):
        return url_to_df(url, exp_jsn=exp_jsn, json_col=json_col, keep_nodes=keep_nodes)
    elif isinstance(url, list):
        if len(url) == 1:
            return url_to_df(
                url[0], exp_jsn=exp_jsn, json_col=json_col, keep_nodes=keep_nodes
            )
        else:
            return urls_to_df(
                url, exp_jsn=exp_jsn, json_col=json_col, keep_nodes=keep_nodes
            )
    else:
        raise ValueError("The URL must be a string or list of strings")
