"""Read a zip package and return the contents as a DataFrame."""

from io import StringIO
from tempfile import TemporaryFile
from zipfile import ZipFile, BadZipFile
import pandas as pd


def read_zip_pack(resp: bytes) -> pd.DataFrame:
    """
    Reads a zip file and returns the contents as a DataFrame.

    Args:
        resp (bytes): The raw content of the zip file.

    Returns:
        pd.DataFrame: The contents of the zip file as a DataFrame.

    Raises:
        ValueError: If the URL does not contain a zip file.
        BadZipFile: If the zip file is not valid.
    """
    # if resp.headers.get_content_type() != "application/zip":
    #     raise ValueError("The URL used previously does not contain a zip file")

    with TemporaryFile(suffix=".zip") as tf:
        tf.write(resp)
        tf.seek(0)

        try:
            fl_names = ZipFile(tf).namelist()
        except Exception as e:
            raise BadZipFile(
                "The URL used previously does not contain a valid zip file"
            )

        if len(fl_names) == 0:
            raise ValueError("The zip archive is empty")

        nm = fl_names[0]  # take only the first file

        with ZipFile(tf) as zp:
            with zp.open(nm) as f:
                fl_content = f.read()

                with StringIO(fl_content.decode("utf-8")) as s:
                    df = pd.read_csv(s, sep=",")

    return df
