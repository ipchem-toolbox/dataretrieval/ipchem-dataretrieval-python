"""Process JSON data. """

import json
import pandas as pd
from typing import List, Union

__all__ = ["expand_json", "json_to_df"]


def expand_json(
    obj: Union[pd.DataFrame, List[str], pd.Series],
    json_col: str = None,
    keep_nodes: List[str] = None,
) -> pd.DataFrame:
    """
    Expands a JSON column in a DataFrame or a list of JSON objects.

    Args:
        obj (Union[pd.DataFrame, List[dict], pd.Series]): The object to expand.
        json_col (str, optional): The name of the JSON column in the DataFrame.
        keep_nodes (List[str], optional): The list of JSON nodes to keep.

    Returns:
        pd.DataFrame: The expanded DataFrame.

    Raises:
        ValueError: If the obj parameter is not a DataFrame with a JSON column or a list.
        ValueError: If the json_col parameter is missing or not present in the DataFrame.
        ValueError: If the nodes to keep are not present in the JSON.
    """
    if isinstance(obj, pd.DataFrame):
        is_df = True
        df = obj.copy()
        cols = df.columns

        if json_col is None:
            raise ValueError("The json_col parameter is mandatory for a DataFrame")

        if json_col not in cols:
            raise ValueError(f"The column {json_col} is not present in the DataFrame")

        json_list = df[json_col].to_list()

    elif isinstance(obj, list) or isinstance(obj, pd.Series):
        is_df = False
        json_list = list(obj)

    else:
        raise ValueError(
            "The obj parameter must be a DataFrame with a column with JSON, or a list"
        )

    name_list = list(json.loads(json_list[0]).keys())

    if keep_nodes is not None:
        mask = [nm in keep_nodes for nm in name_list]

        if any(mask) is False:
            raise ValueError("The nodes to keep are not present in the JSON")

        name_list = [i for i, j in zip(name_list, mask) if j]

    record_list = list()

    for rec in json_list:
        rec = json.loads(rec)
        new_rec = {}
        for nm in name_list:
            if rec.get(nm) is None:
                new_rec[nm] = None
            else:
                new_rec[nm] = rec[nm]

        record_list.append(new_rec)

    if is_df:
        df.drop([json_col], axis=1, inplace=True)
        df = pd.concat([df, pd.DataFrame(record_list)], axis=1)
    else:
        df = pd.DataFrame(record_list)

    return df


def json_to_df(jsn: str, pprint: bool = False) -> pd.DataFrame:
    """
    Converts a JSON string to a DataFrame.

    Args:
        jsn (str): The JSON string to convert.
        pprint (bool): Whether to pretty print the JSON string.

    Returns:
        pd.DataFrame: The converted DataFrame.

    Raises:
        ValueError: If the JSON string could not be parsed.
    """
    try:
        dct = json.loads(jsn)
    except Exception as e:
        raise ValueError(f"The JSON string could not be parsed: {e}")

    type_ls = [{"node": k, "value": v, "type": type(v)} for k, v in dct.items()]

    if pprint:
        for rec in type_ls:
            print("Key: ", rec["node"])
            print("\tValue: ", rec["value"])
            print("\tType: ", rec["type"], "\n")

    df = pd.DataFrame(type_ls).set_index("node")

    return df
